create sequence TEST_SEQUENCE start with 1;

insert into bank_user (id, user_name, user_password, user_first_name, user_last_name, user_email, user_phone, role_id)
    values (2, 'test.user', '$2a$10$snXK4qwh2UQpn/SLrTi80OD/xMPQowc0rPqPFiG3WEXb5Vo2rdyKi', 'Test', 'User',
        'test.user@banka.hr', '01414617', 1);
insert into bank_user (id, user_name, user_password, user_first_name, user_last_name, user_email, user_phone, role_id)
    values (3, 'test.user2', '$2a$10$snXK4qwh2UQpn/SLrTi80OD/xMPQowc0rPqPFiG3WEXb5Vo2rdyKi', 'Test', 'User2',
        'test.user2@banka.hr', '01414617', 1);

insert into bank_account (id, account_number, account_balance, user_id)
    values (1, '10000000000', 550.0, 2);
insert into bank_account (id, account_number, account_balance, user_id)
    values (2, '10000000001', 1450.0, 3);

insert into payment_order (id, amount, created, modified, payment_status_id, credit_account_id, debit_account_id)
    values (1, 200.0, '2018-10-19 09:46:34', '2018-10-19 09:46:34', 1, 1, 2);
insert into payment_order (id, amount, created, modified, payment_status_id, credit_account_id, debit_account_id)
    values (2, 250.0, '2018-10-19 09:46:34', '2018-10-19 09:46:34', 2, 1, 2);
insert into payment_order (id, amount, created, modified, payment_status_id, credit_account_id, debit_account_id)
    values (3, 800.0, '2018-10-19 09:46:34', '2018-10-19 09:46:34', 1, 1, 2);