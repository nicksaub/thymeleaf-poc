package hr.nsaub.spring.boot.thmeleaf.test.repos;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.ThymeleafPocApplication;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankUser;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.BankUserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ThymeleafPocApplication.class})
public class BankUserRepositoyTest {

    @Autowired
    private BankUserRepository bankUserRepository;

    @Test
    public void testGetUserByUserName(){
        BankUser bankUser = bankUserRepository.getUserByUserName("banka");

        Assert.assertNotNull(bankUser);
    }
}
