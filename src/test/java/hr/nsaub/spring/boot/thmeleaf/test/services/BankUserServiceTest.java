package hr.nsaub.spring.boot.thmeleaf.test.services;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.ThymeleafPocApplication;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common.Constants;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.BankUserDTO;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.services.BankUserService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ThymeleafPocApplication.class})
public class BankUserServiceTest {

    @Autowired
    private BankUserService bankUserService;

    @Test
    public void testCreateBankUser(){
        BankUserDTO bankUserDTO = new BankUserDTO();
        bankUserDTO.setUserName("nikola.saub");
        bankUserDTO.setUserPassword("Vortexrikes1");
        bankUserDTO.setUserPasswordRepeat("Vortexrikes1");
        bankUserDTO.setUserFirstName("Nikola");
        bankUserDTO.setUserLastName("Šaub");
        bankUserDTO.setUserPhone("01414617");
        bankUserDTO.setUserEmail("nikola.saub@mail.com");

        bankUserDTO = bankUserService.createBankUser(bankUserDTO, Constants.ROLE_USER_ID);

        Assert.assertNotNull("Bank user returned as null!", bankUserDTO);
        Assert.assertNotNull("Bank user not persisted!", bankUserDTO.getId());
        Assert.assertFalse("Account number not returned!", StringUtils.isEmpty(bankUserDTO.getAccountNumber()));
        Assert.assertTrue("Account number not numeric!", StringUtils.isNumeric(bankUserDTO.getAccountNumber()));
        Assert.assertTrue("Account number length invalid!",bankUserDTO.getAccountNumber().length() == 10);
    }
}
