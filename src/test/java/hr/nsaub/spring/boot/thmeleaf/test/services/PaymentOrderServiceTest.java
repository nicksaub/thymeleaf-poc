package hr.nsaub.spring.boot.thmeleaf.test.services;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.ThymeleafPocApplication;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common.Constants;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.PaymentOrderDTO;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankAccount;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.BankAccountRepository;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.services.PaymentOrderService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ThymeleafPocApplication.class})
public class PaymentOrderServiceTest {

    @Autowired
    private PaymentOrderService paymentOrderService;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Test
    public void testGetPaymentOrdersForUser() {
        List<PaymentOrderDTO> results = paymentOrderService.getPaymentOrdersForUser(2, 1, 0, 5);

        Assert.assertFalse(results.isEmpty());
    }

    @Test
    public void testCreateOrder() {
        PaymentOrderDTO result = paymentOrderService.createOrder("10000000000", "10000000001",
            new BigDecimal("456.0"));

        Assert.assertNotNull(result);
        Assert.assertTrue(result.getId()>0);
        Assert.assertTrue(result.getStatus().equals(Constants.PAYMENT_STATUS_CREATED_ID));
        Assert.assertTrue(result.getAmount().compareTo(new BigDecimal("456.0")) == 0);
    }

    @Test
    public void testCreateOrder_autoexecute() {
        PaymentOrderDTO result = paymentOrderService.createOrder("10000000000", "10000000001",
            new BigDecimal("50.0"));

        Assert.assertNotNull(result);
        Assert.assertTrue(result.getId()>0);
        Assert.assertTrue(result.getStatus().equals(Constants.PAYMENT_STATUS_EXECUTED_ID));
        Assert.assertTrue(result.getAmount().compareTo(new BigDecimal("50.0")) == 0);
    }

    @Test
    public void testGetPaymentOrderDetails() {
        PaymentOrderDTO result = paymentOrderService.getPaymentOrderDetails(1);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getAmount());
        Assert.assertNotNull(result.getStatus());
        Assert.assertTrue(StringUtils.isNotBlank(result.getCreditAccount()));
        Assert.assertTrue(StringUtils.isNotBlank(result.getDebitAccount()));
        Assert.assertTrue(StringUtils.isNotBlank(result.getPayeeFirstName()));
        Assert.assertTrue(StringUtils.isNotBlank(result.getPayeeLastName()));
        Assert.assertTrue(StringUtils.isNotBlank(result.getPayerFirstName()));
        Assert.assertTrue(StringUtils.isNotBlank(result.getPayerLastName()));
    }

    @Test
    public void testExecutePaymentOrder() {
        PaymentOrderDTO result = paymentOrderService.executePaymentOrder(1);
        BankAccount creditAccount = bankAccountRepository.findById(1).get();
        BankAccount debitAccount = bankAccountRepository.findById(2).get();

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getAmount());
        Assert.assertNotNull(result.getStatus());
        Assert.assertTrue(result.getStatus().equals(Constants.PAYMENT_STATUS_EXECUTED_ID));
        Assert.assertTrue(creditAccount.getAccountBalance().compareTo(new BigDecimal("300.0")) == 0);
        Assert.assertTrue(debitAccount.getAccountBalance().compareTo(new BigDecimal("1700.0")) == 0);
    }

    @Test
    public void testDeclinePaymentOrder() {
        PaymentOrderDTO result = paymentOrderService.declinePaymentOrder(3);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getStatus());
        Assert.assertTrue(result.getStatus().equals(Constants.PAYMENT_STATUS_DECLINED_ID));
    }
}
