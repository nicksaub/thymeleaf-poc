package hr.nsaub.spring.boot.thmeleaf.test.repos;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.ThymeleafPocApplication;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.PaymentOrder;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.PaymentOrderRepositoryCustom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ThymeleafPocApplication.class})
public class PamentOrderRepositoryCustomTest {

    @Autowired
    private PaymentOrderRepositoryCustom paymentOrderRepositoryCustom;

    @Test
    public void testGetPaymentOrdersForUser1(){
        List<PaymentOrder> results = paymentOrderRepositoryCustom.getPaymentOrders(2, null, null, null);

        Assert.assertTrue(results.size() == 3);
    }

    @Test
    public void testGetPaymentOrdersForUser2(){
        List<PaymentOrder> results = paymentOrderRepositoryCustom.getPaymentOrders(2, 1, null, null);

        Assert.assertTrue(results.size() == 2);
    }

    @Test
    public void testGetPaymentOrdersForUser3(){
        List<PaymentOrder> results = paymentOrderRepositoryCustom.getPaymentOrders(2, 2, null, null);

        Assert.assertTrue(results.size() == 1);
    }

    @Test
    public void testGetPaymentOrdersForUser4(){
        List<PaymentOrder> results = paymentOrderRepositoryCustom.getPaymentOrders(null, null, null, null);

        Assert.assertTrue(results.size() > 0);
    }

    @Test
    public void testGetPaymentOrdersForUser4_1(){
        List<PaymentOrder> results = paymentOrderRepositoryCustom.getPaymentOrders(null, null, 0, 5);

        Assert.assertTrue(results.size() > 0);
    }

    @Test
    public void testGetPaymentOrderCount(){
        Long count = paymentOrderRepositoryCustom.getPaymentOrderCount(null, null);

        Assert.assertTrue(count > 0);
    }
}
