package hr.nsaub.spring.boot.thmeleaf.test.repos;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.ThymeleafPocApplication;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.SequenceRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ThymeleafPocApplication.class})
public class SequenceRepositoryTest {

    @Autowired
    private SequenceRepository sequenceRepository;

    @Test
    public void testGetNextValueForSequence() {
        BigInteger val = sequenceRepository.getNextValueForSequence("TEST_SEQUENCE");

        Assert.assertTrue(val.longValue() > 0L);
    }
}
