package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.PaymentOrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * PaymentOrder JPA repository.
 *
 * @author nikola.saub
 *
 */
public interface PaymentOrderRepository extends JpaRepository<PaymentOrder, Integer> {
}
