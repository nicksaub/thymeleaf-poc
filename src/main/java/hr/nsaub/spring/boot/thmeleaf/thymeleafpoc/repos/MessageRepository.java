package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * Message JPA repository.
 *
 * @author nikola.saub
 *
 */
public interface MessageRepository extends JpaRepository<Message, Integer>{
}
