package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * BankUser JPA repository.
 *
 * @author nikola.saub
 *
 */
public interface BankUserRepository extends JpaRepository<BankUser, Integer> {

    /**
     * Loads a user and its role by the given username.
     * @param userName
     * @return
     */
    @Query("select u from BankUser u left join fetch u.role r where u.userName = :userName")
    BankUser getUserByUserName(@Param("userName") String userName);
}
