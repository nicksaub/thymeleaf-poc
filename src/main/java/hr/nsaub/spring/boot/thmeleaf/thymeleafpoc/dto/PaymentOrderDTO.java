package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common.Constants;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * Data transfer object for PaymentOrder entity.
 *
 * @author nikola.saub
 *
 */
public class PaymentOrderDTO implements Serializable{
    private Integer id;
    private BigDecimal amount;
    private LocalDateTime created;
    private LocalDateTime modified;
    private Integer status;
    private String statusName;
    private String payerFirstName;
    private String payerLastName;
    private String payerPhone;
    private String payerEmail;
    private String creditAccount;
    private String payeeFirstName;
    private String payeeLastName;
    private String payeePhone;
    private String payeeEmail;
    private String debitAccount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    public String getPayerFirstName() {
        return payerFirstName;
    }

    public void setPayerFirstName(String payerFirstName) {
        this.payerFirstName = payerFirstName;
    }

    public String getPayerLastName() {
        return payerLastName;
    }

    public void setPayerLastName(String payerLastName) {
        this.payerLastName = payerLastName;
    }

    public String getPayeeFirstName() {
        return payeeFirstName;
    }

    public void setPayeeFirstName(String payeeFirstName) {
        this.payeeFirstName = payeeFirstName;
    }

    public String getPayeeLastName() {
        return payeeLastName;
    }

    public void setPayeeLastName(String payeeLastName) {
        this.payeeLastName = payeeLastName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getPayerPhone() {
        return payerPhone;
    }

    public void setPayerPhone(String payerPhone) {
        this.payerPhone = payerPhone;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public String getPayeePhone() {
        return payeePhone;
    }

    public void setPayeePhone(String payeePhone) {
        this.payeePhone = payeePhone;
    }

    public String getPayeeEmail() {
        return payeeEmail;
    }

    public void setPayeeEmail(String payeeEmail) {
        this.payeeEmail = payeeEmail;
    }

    /**
     * Returns a CSS class coresponding to the status.
     * @return
     */
    public String getStyle(){
        String retval = "";

        if(status.equals(Constants.PAYMENT_STATUS_CREATED_ID)){
            retval = "order_created";
        }
        else if(status.equals(Constants.PAYMENT_STATUS_DECLINED_ID)){
            retval = "order_declined";
        }
        else if(status.equals(Constants.PAYMENT_STATUS_EXECUTED_ID)){
            retval = "order_executed";
        }

        return retval;
    }
}
