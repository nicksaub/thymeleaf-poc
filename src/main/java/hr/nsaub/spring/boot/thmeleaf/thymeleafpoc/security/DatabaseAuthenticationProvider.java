package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.security;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankUser;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.BankUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class DatabaseAuthenticationProvider implements AuthenticationProvider{

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private BankUserRepository bankUserRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = authentication.getName();
        String password = authentication.getCredentials().toString();
        BankUser bankUser = bankUserRepository.getUserByUserName(userName);

        if(bankUser != null && bCryptPasswordEncoder.matches(password, bankUser.getUserPassword())) {
            return new UsernamePasswordAuthenticationToken(bankUser, password,
                Arrays.asList(()->bankUser.getRole().getName()));
        }
        else{
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
