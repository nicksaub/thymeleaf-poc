package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class BankAccount {
    private Integer id;
    private String accountNumber;
    private BigDecimal accountBalance;

    private BankUser bankUser;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public BankUser getBankUser() {
        return bankUser;
    }

    public void setBankUser(BankUser bankUser) {
        this.bankUser = bankUser;
    }
}
