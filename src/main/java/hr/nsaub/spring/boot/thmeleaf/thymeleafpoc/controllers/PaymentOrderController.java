package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.controllers;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common.Constants;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.PaymentOrderDTO;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSMessage;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSResponse;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.exceptions.ServiceException;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankUser;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.services.PaymentOrderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Principal;
import java.util.List;

@Controller
@SessionScope
public class PaymentOrderController {
    private static final Logger LOG = LoggerFactory.getLogger(PaymentOrderController.class);

    @Autowired
    private PaymentOrderService paymentOrderService;

    private List<PaymentOrderDTO> paymentOrderDTOs;

    /**
     * Renders the order list view.
     *
     * @param pageNo
     * @param sortBy
     * @param ascending
     * @param statusId
     * @param principal
     * @param model
     * @return
     */
    @GetMapping(value = "/payment_orders")
    public String showPaymentOrders(@RequestParam(name = "pageNo", required = false)
                                        Integer pageNo,
                                    @RequestParam(name = "sortBy", required = false)
                                        String sortBy,
                                    @RequestParam(name = "ascending", required = false)
                                        Boolean ascending,
                                    @RequestParam(name = "status", required = false)
                                        Integer statusId,
                                    Principal principal, ModelMap model){
        BankUser bankUser = (BankUser)((UsernamePasswordAuthenticationToken)principal).getPrincipal();
        Boolean isAdmin = bankUser.getRole().getId().equals(Constants.ROLE_ADMIN_ID);

        prepareOrderListModel(isAdmin, bankUser.getId(), pageNo, sortBy, ascending, statusId, new WSResponse(), model);

        return "payment_orders";
    }

    /**
     * Renders a AJAX response for order details.
     *
     * @param orderId
     * @param model
     * @return
     */
    @GetMapping(value = "/payment_orders/payment_order_details")
    @ResponseBody
    public ModelAndView showPaymentOrderDetails(@RequestParam(name = "orderId")
                                                    Integer orderId, ModelMap model) {
        PaymentOrderDTO paymentOrderDTO = paymentOrderService.getPaymentOrderDetails(orderId);
        model.addAttribute("paymentOrderDTO", paymentOrderDTO);

        return new ModelAndView("/payment_order_details", model);
    }

    /**
     * Handles the save order action.
     *
     * @param request
     * @param principal
     * @param model
     * @return
     */
    @PostMapping(value = "/payment_orders/saveOrder")
    public ModelAndView saveOrder(HttpServletRequest request, Principal principal, ModelMap model) {
        BankUser bankUser = (BankUser)((UsernamePasswordAuthenticationToken)principal).getPrincipal();
        Boolean isAdmin = bankUser.getRole().getId().equals(Constants.ROLE_ADMIN_ID);
        WSResponse wsResponse = new WSResponse();
        ModelAndView modelAndView = new ModelAndView("/payment_orders", model);
        String creditAccount = bankUser.getBankAccount().getAccountNumber();
        String amount = request.getParameter("amount");
        String debitAccount = request.getParameter("debitAccount");

        if(StringUtils.isBlank(debitAccount)){
            wsResponse.getErrors().add(new WSMessage(WSResponse.ERROR_MESSAGES.ERROR_CODE_009.getCode(),
                WSResponse.ERROR_MESSAGES.ERROR_CODE_009.getMessage()));
        }
        BigDecimal amountBD = null;
        try {
            amountBD = new BigDecimal(StringUtils.isNotBlank(amount) ? amount.replace(',', '.') : amount);
        } catch (NumberFormatException e) {
            wsResponse.getErrors().add(new WSMessage(WSResponse.ERROR_MESSAGES.ERROR_CODE_010.getCode(),
                WSResponse.ERROR_MESSAGES.ERROR_CODE_010.getMessage()));
        }

        if(wsResponse.getErrors().isEmpty()) {
            try {
                paymentOrderService.createOrder(creditAccount, debitAccount, amountBD);
            } catch (ServiceException e) {
                wsResponse.getErrors().addAll(e.getWsReponse().getErrors());
            } catch (RuntimeException e) {
                LOG.error(e.getMessage(), e);
                model.addAttribute("error", "System error happened! Please try again.");
            }
        }

        prepareOrderListModel(isAdmin, bankUser.getId(), null, null, null, null, wsResponse, model);

        return modelAndView;
    }

    /**
     * Handles the execute order action.
     *
     * @param orderId
     * @param principal
     * @param model
     * @return
     */
    @PostMapping(value = "/payment_orders/executeOrder")
    public ModelAndView executeOrder(@RequestParam(name = "orderId")
                                         Integer orderId,
                                     Principal principal, ModelMap model) {
        BankUser bankUser = (BankUser)((UsernamePasswordAuthenticationToken)principal).getPrincipal();
        Boolean isAdmin = bankUser.getRole().getId().equals(Constants.ROLE_ADMIN_ID);
        WSResponse wsResponse = new WSResponse();
        ModelAndView modelAndView = new ModelAndView("/payment_orders", model);

        try {
            paymentOrderService.executePaymentOrder(orderId);
            wsResponse.getInfos().add(new WSMessage(WSResponse.INFO_MESSAGES.INFO_CODE_001.getCode(),
                WSResponse.INFO_MESSAGES.INFO_CODE_001.getMessage()));
        } catch (RuntimeException e) {
            LOG.error(e.getMessage(), e);
            model.addAttribute("error", "System error happened! Please try again.");
        }

        prepareOrderListModel(isAdmin, bankUser.getId(), null, null, null, null, wsResponse, model);

        return modelAndView;
    }

    /**
     * Handles the decline order action.
     *
     * @param orderId
     * @param principal
     * @param model
     * @return
     */
    @PostMapping(value = "/payment_orders/declineOrder")
    public ModelAndView declineOrder(@RequestParam(name = "orderId")
                                         Integer orderId,
                                     Principal principal, ModelMap model) {
        BankUser bankUser = (BankUser)((UsernamePasswordAuthenticationToken)principal).getPrincipal();
        Boolean isAdmin = bankUser.getRole().getId().equals(Constants.ROLE_ADMIN_ID);
        WSResponse wsResponse = new WSResponse();
        ModelAndView modelAndView = new ModelAndView("/payment_orders", model);

        try {
            paymentOrderService.declinePaymentOrder(orderId);
            wsResponse.getInfos().add(new WSMessage(WSResponse.INFO_MESSAGES.INFO_CODE_002.getCode(),
                WSResponse.INFO_MESSAGES.INFO_CODE_002.getMessage()));
        } catch (RuntimeException e) {
            LOG.error(e.getMessage(), e);
            model.addAttribute("error", "System error happened! Please try again.");
        }

        prepareOrderListModel(isAdmin, bankUser.getId(), null, null, null, null, wsResponse, model);

        return modelAndView;
    }

    /**
     * Prepares the view model for order list view.
     *
     * @param isAdmin
     * @param userId
     * @param pageNo
     * @param sortBy
     * @param ascending
     * @param statusId
     * @param wsResponse
     * @param model
     */
    private void prepareOrderListModel(Boolean isAdmin, Integer userId, Integer pageNo, String sortBy, Boolean ascending,
                                       Integer statusId, WSResponse wsResponse, ModelMap model){
        if(pageNo!=null){
            paymentOrderDTOs = paymentOrderService.getPaymentOrdersForUser(isAdmin ? null : userId,
                statusId == null || statusId == 0 ? null : statusId, pageNo * Constants.PAGE_SIZE, Constants.PAGE_SIZE);
        }
        else{
            paymentOrderDTOs = paymentOrderService.getPaymentOrdersForUser(isAdmin ? null : userId,
                statusId == null || statusId == 0 ? null : statusId, 0, Constants.PAGE_SIZE);
        }

        Integer pageCount = BigDecimal.valueOf(paymentOrderService.getPaymentOrderCount(isAdmin ? null : userId, statusId) / Constants.PAGE_SIZE.doubleValue())
            .setScale(0, RoundingMode.UP).intValue();

        model.addAttribute("pageCount", pageCount);
        model.addAttribute("paymentOrderDTOs", paymentOrderDTOs);
        model.addAttribute("ascending", ascending != null ? ascending : true);
        model.addAttribute("sortBy", StringUtils.isNotBlank(sortBy) ? sortBy : "id");
        model.addAttribute("currentPage", pageNo != null ? pageNo : 0);
        model.addAttribute("status", statusId != null ? statusId : 0);
        model.addAttribute("response", wsResponse);
        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("activeView", "payment_orders");
    }
}
