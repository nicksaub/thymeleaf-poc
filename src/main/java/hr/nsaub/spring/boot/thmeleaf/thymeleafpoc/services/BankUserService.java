package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.services;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common.Constants;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.BankUserDTO;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSMessage;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSResponse;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.exceptions.ServiceException;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.mappers.BankUserMapper;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankAccount;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankUser;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.Role;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.BankAccountRepository;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.BankUserRepository;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.SequenceRepository;
import ma.glasnost.orika.BoundMapperFacade;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Service containing bank user business logic.
 *
 * @author nikola.saub
 *
 */
@Service
@Transactional
public class BankUserService {

    @Autowired
    private SequenceRepository sequenceRepository;

    @Autowired
    private BankUserRepository bankUserRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Creates a banking app user with the given role. If it is a 'user' role also creates a bank account.
     * @param bankUserDTO
     * @param roleId
     * @return
     */
    public BankUserDTO createBankUser(BankUserDTO bankUserDTO, Integer roleId){
        validateUser(bankUserDTO);
        bankUserDTO.setUserPassword(bCryptPasswordEncoder.encode(bankUserDTO.getUserPassword()));
        BoundMapperFacade<BankUser, BankUserDTO> mapper = BankUserMapper.getMapperFacade(BankUser.class,
            BankUserDTO.class);
        BankUser bankUser = mapper.mapReverse(bankUserDTO);

        bankUser.setRole(new Role(roleId));
        bankUser = bankUserRepository.save(bankUser);

        BankAccount bankAccount = null;
        if(roleId.equals(Constants.ROLE_USER_ID)) {
            bankAccount = new BankAccount();
            DecimalFormat df = new DecimalFormat("0000000000");
            //fetches the next free account number
            BigInteger val = sequenceRepository.getNextValueForSequence("account_number_sequence");
            if (val.longValue() > 9999999999L) {
                throw new ServiceException(new WSResponse(WSResponse.STATUS_ERROR, "999",
                    "Account numbers are spent!"));
            }
            bankAccount.setAccountNumber(df.format(val.intValue()));
            bankAccount.setAccountBalance(Constants.INITIAL_BALANCE);
            bankAccount.setBankUser(bankUser);
            bankAccount = bankAccountRepository.save(bankAccount);
        }

        BankUserDTO retval = mapper.map(bankUser);
        if(bankAccount != null) {
            retval.setAccountNumber(bankAccount.getAccountNumber());
        }

        return retval;
    }

    /**
     * Validates the given user against the business rules.
     * @param bankUserDTO
     */
    private void validateUser(BankUserDTO bankUserDTO){
        List<WSMessage> errors = new ArrayList<>();

        if(StringUtils.isEmpty(bankUserDTO.getUserName())){
            errors.add(new WSMessage("001", "Username missing!"));
        }
        else if(bankUserRepository.getUserByUserName(bankUserDTO.getUserName())!=null){
            errors.add(new WSMessage("002", "User with a username '" +
                bankUserDTO.getUserName() + "' already exists!"));
        }
        if(StringUtils.isEmpty(bankUserDTO.getUserPassword())){
            errors.add(new WSMessage("003", "Password is missing!"));
        }
        else if(!StringUtils.isAlphanumeric(bankUserDTO.getUserPassword()) || bankUserDTO.getUserPassword().length()<8
            || bankUserDTO.getUserPassword().toLowerCase().equals(bankUserDTO.getUserPassword())){
            errors.add(new WSMessage("004", "Password must be at least 8 characters long, "
                + "have at least one capital letter and be a mix of letters and digits!"));
        }
        else if(!bankUserDTO.getUserPassword().equals(bankUserDTO.getUserPasswordRepeat())){
            errors.add(new WSMessage("005", "Password and the repeated password don't match!"));
        }
        if(StringUtils.isBlank(bankUserDTO.getUserEmail())){
            errors.add(new WSMessage("006", "E-mail is missing!"));
        }

        if(!errors.isEmpty()){
            WSResponse wsReponse = new WSResponse(WSResponse.STATUS_ERROR);
            wsReponse.setErrors(errors);

            throw new ServiceException(wsReponse);
        }
    }
}
