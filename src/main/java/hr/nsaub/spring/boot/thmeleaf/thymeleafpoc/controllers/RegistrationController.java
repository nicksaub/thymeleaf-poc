package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.controllers;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common.Constants;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.BankUserDTO;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSResponse;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.exceptions.ServiceException;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.services.BankUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestScope
public class RegistrationController {
    private static final Logger LOG = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    private BankUserService bankUserService;

    /**
     * Navigates to the registration view.
     * @return
     */
    @GetMapping(value = "/registration")
    public ModelAndView showRegistration(ModelMap model){
        ModelAndView modelAndView = new ModelAndView("/registration", model);
        model.addAttribute("response", new WSResponse());
        model.addAttribute("bankUserDTO", new BankUserDTO());
        return modelAndView;
    }

    /**
     * Procesess the user registration form and replies with the response.
     * @param request
     * @param model
     * @return
     */
    @PostMapping(value = "/registration/saveUser")
    public ModelAndView saveUser(HttpServletRequest request, ModelMap model) {
        ModelAndView modelAndView;
        BankUserDTO bankUserDTO = new BankUserDTO();
        bankUserDTO.setUserName(request.getParameter("userName"));
        bankUserDTO.setUserPassword(request.getParameter("userPassword"));
        bankUserDTO.setUserPasswordRepeat(request.getParameter("userPasswordRepeat"));
        bankUserDTO.setUserFirstName(request.getParameter("userFirstName"));
        bankUserDTO.setUserLastName(request.getParameter("userLastName"));
        bankUserDTO.setUserEmail(request.getParameter("userEmail"));
        bankUserDTO.setUserPhone(request.getParameter("userPhone"));

        try {
            bankUserDTO = bankUserService.createBankUser(bankUserDTO, Constants.ROLE_USER_ID);
            model.addAttribute("info", "User with user name '" + bankUserDTO.getUserName() +
                "' created! Please login with your password.");
            modelAndView = new ModelAndView("/login", model);
        } catch (ServiceException e) {
            model.addAttribute("response", e.getWsReponse());
            model.addAttribute("bankUserDTO", bankUserDTO);
            modelAndView = new ModelAndView("/registration", model);
        } catch (RuntimeException e) {
            LOG.error(e.getMessage(), e);
            model.addAttribute("error", "System error happened! Please try again.");
            modelAndView = new ModelAndView("/registration", model);
        }

        return modelAndView;
    }
}
