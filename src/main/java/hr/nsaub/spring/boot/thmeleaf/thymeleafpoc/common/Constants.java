package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common;

import java.math.BigDecimal;

/**
 *
 * A class containing system constants.
 *
 * @author nikola.saub
 *
 */
public class Constants {
    /**
     * Default page size.
     */
    public static final Integer PAGE_SIZE = 5;
    /**
     * Default initial balance for a created bank account.
     */
    public static final BigDecimal INITIAL_BALANCE = BigDecimal.valueOf(1000.0);
    /**
     * Maximum amount for which a created payment order is auto-executed.
     */
    public static final BigDecimal AUTO_EXECUTE_CUTOFF_AMOUNT = BigDecimal.valueOf(100.0);
    /**
     * Defines every n-th created payment order that si to be left unexecuted.
     */
    public static final Integer AUTO_EXECUTE_PREVENTION_RATE = 10;
    /**
     * Id for the role 'User'.
     */
    public static final Integer ROLE_USER_ID = 1;
    /**
     * Id for the role 'Admin'.
     */
    public static final Integer ROLE_ADMIN_ID = 2;
    /**
     * Id for the payment status 'Created'.
     */
    public static final Integer PAYMENT_STATUS_CREATED_ID = 1;
    /**
     * Id for the payment status 'Exexcuted'.
     */
    public static final Integer PAYMENT_STATUS_EXECUTED_ID = 2;
    /**
     * Id for the payment status 'Declined'.
     */
    public static final Integer PAYMENT_STATUS_DECLINED_ID = 3;
}
