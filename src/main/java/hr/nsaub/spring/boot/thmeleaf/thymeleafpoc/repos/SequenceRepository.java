package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;

/**
 *
 * A repository used for manipulating database sequences.
 *
 * @author nikola.saub
 *
 */
@Repository
public class SequenceRepository {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Returns the next value from the given sequence.
     * @param sequenceName
     * @return
     */
    public BigInteger getNextValueForSequence(String sequenceName){
        Query query = entityManager.createNativeQuery("call NEXT VALUE FOR " + sequenceName);

        return (BigInteger) query.getSingleResult();
    }
}
