package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.PaymentOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * Custom PaymentOrder repository containing more complex JPA methods.
 *
 * @author nikola.saub
 *
 */
@Repository
public class PaymentOrderRepositoryCustom{

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Loads payment orders for the given user and an optional status filter.
     * @param userId
     * @param statusId
     * @param firstRow
     *@param pageSize @return
     */
    public List<PaymentOrder> getPaymentOrders(Integer userId, Integer statusId, Integer firstRow, Integer pageSize){
        Query query = entityManager.createQuery(buildPaymentOrderQuery(false, userId, statusId));
        if(firstRow!=null && pageSize!=null){
            query.setFirstResult(firstRow);
            query.setMaxResults(pageSize);
        }

        if(userId != null) {
            query.setParameter("userId", userId);
        }
        if(statusId != null){
            query.setParameter("statusId", statusId);
        }

        return query.getResultList();
    }

    /**
     * Counts all orders for the given filter.
     * @param userId
     * @param statusId
     * @return
     */
    public Long getPaymentOrderCount(Integer userId, Integer statusId){
        Query query = entityManager.createQuery(buildPaymentOrderQuery(true, userId, statusId));

        if(userId != null) {
            query.setParameter("userId", userId);
        }
        if(statusId != null){
            query.setParameter("statusId", statusId);
        }

        return (Long)query.getSingleResult();
    }

    /**
     * Load a payment order with the given id and fetches all the related records.
     * @param orderId
     * @return
     */
    public PaymentOrder getPaymentOrderDetails(Integer orderId){
        if(orderId == null){
            throw new IllegalArgumentException("Order id is required!");
        }

        StringBuilder hql = new StringBuilder("select po from PaymentOrder po ")
            .append("left join fetch po.paymentStatus ps left join fetch po.creditAccount ca ")
            .append("left join fetch po.debitAccount da left join fetch ca.bankUser cbu ")
            .append("left join fetch ca.bankUser dbu where po.id = :orderId");

        Query query = entityManager.createQuery(hql.toString());
        query.setParameter("orderId", orderId);

        return (PaymentOrder)query.getSingleResult();
    }

    /**
     * Builds the payment order query.
     * @param count
     * @param userId
     * @param statusId
     * @return
     */
    private String buildPaymentOrderQuery(Boolean count, Integer userId, Integer statusId){
        StringBuilder hql = new StringBuilder("select ").append(count ? "count(po.id)" : "po")
            .append(" from PaymentOrder po ")
            .append("left join ").append(count ? "" : "fetch").append(" po.paymentStatus ps left join ")
            .append(count ? "" : "fetch").append(" po.creditAccount ca ")
            .append("left join ").append(count ? "" : "fetch")
            .append(" po.debitAccount da left join ").append(count ? "" : "fetch")
            .append(" ca.bankUser bu where 1=1 ");
        if(userId != null){
            hql.append("and bu.id = :userId ");
        }
        if(statusId != null){
            hql.append("and ps.id = :statusId ");
        }
        if(!count) {
            hql.append("order by po.created");
        }

        return hql.toString();
    }
}
