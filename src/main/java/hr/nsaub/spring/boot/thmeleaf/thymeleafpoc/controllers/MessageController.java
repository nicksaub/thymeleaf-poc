package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.controllers;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common.Constants;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.Message;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSResponse;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.MessageRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@SessionScope
public class MessageController {
    private static final Logger LOG = LoggerFactory.getLogger(MessageController.class);
    private Page<Message> messages;
    private Integer selectedMessageId;

    @Autowired
    private MessageRepository messageRepository;

    @PostConstruct
    public void init() {
    }

    @GetMapping(value = "/messages")
    public String showMessages(@RequestParam(name = "pageNo", required = false)
                                   Integer pageNo,
                               @RequestParam(name = "sortBy", required = false)
                                   String sortBy,
                               @RequestParam(name = "ascending", required = false)
                                   Boolean ascending,
                               Model model) {
        if (pageNo != null) {
            messages = messageRepository.findAll(PageRequest.of(pageNo, Constants.PAGE_SIZE,
                new Sort(ascending != null ? (ascending ? Sort.Direction.ASC : Sort.Direction.DESC) :
                    Sort.Direction.ASC, StringUtils.isNotBlank(sortBy) ? sortBy : "id")));
        } else {
            messages = messageRepository.findAll(PageRequest.of(0, Constants.PAGE_SIZE,
                new Sort(ascending != null ? (ascending ? Sort.Direction.ASC : Sort.Direction.DESC) :
                    Sort.Direction.ASC, StringUtils.isNotBlank(sortBy) ? sortBy : "id")));
        }

        Integer pageCount = BigDecimal.valueOf(messages.getTotalElements() / Constants.PAGE_SIZE.doubleValue())
            .setScale(0, RoundingMode.UP).intValue();

        model.addAttribute("pageCount", pageCount);
        model.addAttribute("myMessages", messages);
        model.addAttribute("ascending", ascending != null ? ascending : true);
        model.addAttribute("sortBy", StringUtils.isNotBlank(sortBy) ? sortBy : "id");
        model.addAttribute("currentPage", pageNo != null ? pageNo : 0);
        model.addAttribute("activeView", "messages");

        return "messages";
    }

    @PostMapping(value = "/messages/saveMessage")
    public ModelAndView saveMessage(HttpServletRequest request, ModelMap model) {
        Message message = new Message();
        message.setMessage(request.getParameter("message"));
        message.setTimestamp(new Date());
        message.setStatus(Message.MessageStatus.UNREAD.name());

        messageRepository.save(message);

        return new ModelAndView("redirect:/messages", model);
    }

    @GetMapping(value = "messages/handleSelect")
    @ResponseBody
    public WSResponse handleSelect(Integer selectedMessageId) {
        this.selectedMessageId = selectedMessageId;

        return new WSResponse(WSResponse.STATUS_OK);
    }

    @PostMapping(value = "/messages/deleteSelectedMessage")
    public ModelAndView deleteSelectedMessage(ModelMap model) {
        messageRepository.delete(new Message(selectedMessageId));

        return new ModelAndView("redirect:/messages", model);
    }

    @PostMapping(value = "messages/updateDate")
    @ResponseBody
    public WSResponse updateDate(Integer selectedMessageId, String selectedDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.");
            Optional<Message> message = messageRepository.findById(selectedMessageId);
            message.get().setTimestamp(sdf.parse(selectedDate));

            messageRepository.save(message.get());

            return new WSResponse(WSResponse.STATUS_OK);
        } catch (ParseException e) {
            LOG.error(e.getMessage(), e);
            return new WSResponse(WSResponse.STATUS_ERROR);
        } catch (RuntimeException e) {
            LOG.error(e.getMessage(), e);
            return new WSResponse(WSResponse.STATUS_ERROR);
        }
    }
}
