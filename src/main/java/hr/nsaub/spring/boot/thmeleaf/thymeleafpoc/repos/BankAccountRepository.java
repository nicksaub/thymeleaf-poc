package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * BankAccount JPA repository.
 *
 * @author nikola.saub
 *
 */
public interface BankAccountRepository  extends JpaRepository<BankAccount, Integer> {

    /**
     * Loads a bank account by its number.
     * @param accountNumber
     * @return
     */
    BankAccount findByAccountNumber(String accountNumber);
}
