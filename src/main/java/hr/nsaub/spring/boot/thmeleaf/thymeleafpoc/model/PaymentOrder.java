package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class PaymentOrder {
    private Integer id;
    private BigDecimal amount;
    private LocalDateTime created;
    private LocalDateTime modified;

    private PaymentStatus paymentStatus;
    private BankAccount creditAccount;
    private BankAccount debitAccount;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @ManyToOne
    @JoinColumn(name = "payment_status_id", referencedColumnName = "id", nullable = false)
    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @ManyToOne
    @JoinColumn(name = "credit_account_id", referencedColumnName = "id", nullable = false)
    public BankAccount getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(BankAccount creditAccount) {
        this.creditAccount = creditAccount;
    }

    @ManyToOne
    @JoinColumn(name = "debit_account_id", referencedColumnName = "id", nullable = false)
    public BankAccount getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(BankAccount debitAccount) {
        this.debitAccount = debitAccount;
    }
}
