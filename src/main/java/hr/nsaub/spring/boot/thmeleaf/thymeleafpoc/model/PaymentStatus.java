package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PaymentStatus {
    private Integer id;
    private String name;

    public PaymentStatus() {
    }

    public PaymentStatus(Integer id) {
        this.id = id;
    }

    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
