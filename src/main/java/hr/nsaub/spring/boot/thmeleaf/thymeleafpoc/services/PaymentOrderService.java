package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.services;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.common.Constants;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.PaymentOrderDTO;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSMessage;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSResponse;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.exceptions.ServiceException;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.mappers.PaymentOrderMapper;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankAccount;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.PaymentOrder;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.PaymentStatus;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.BankAccountRepository;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.PaymentOrderRepository;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.repos.PaymentOrderRepositoryCustom;
import ma.glasnost.orika.BoundMapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Service containing payment order business logic.
 *
 * @author nikola.saub
 *
 */
@Service
@Transactional
public class PaymentOrderService {

    @Autowired
    private PaymentOrderRepository paymentOrderRepository;

    @Autowired
    private PaymentOrderRepositoryCustom paymentOrderRepositoryCustom;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    /**
     * Loads payment orders for the given user and an optional status filter.
     * @param userId
     * @param statusId
     * @return
     */
    public List<PaymentOrderDTO> getPaymentOrdersForUser(Integer userId, Integer statusId, Integer firstRow, Integer pageSize){
        List<PaymentOrder> paymentOrders =  paymentOrderRepositoryCustom.getPaymentOrders(userId, statusId, firstRow, pageSize);
        List<PaymentOrderDTO> paymentOrderDTOs = new ArrayList<>();

        if(!paymentOrders.isEmpty()){
            BoundMapperFacade<PaymentOrder, PaymentOrderDTO> mapper = PaymentOrderMapper
                .getMapperFacade(PaymentOrder.class, PaymentOrderDTO.class);

            for(PaymentOrder paymentOrder : paymentOrders){
                paymentOrderDTOs.add(mapper.map(paymentOrder));
            }
        }

        return paymentOrderDTOs;
    }

    /**
     * Counts all orders for the given filter.
     * @param userId
     * @param statusId
     * @return
     */
    public Long getPaymentOrderCount(Integer userId, Integer statusId){
        return paymentOrderRepositoryCustom.getPaymentOrderCount(userId, statusId);
    }

    /**
     * Creates payment order with the given account numbers nad amount. If the amount is less than or equal 100 then
     * auto-execute the order. Leaves every tenth order unexecuted.
     * @param creditAccountNumber
     * @param debitAccountNumber
     * @param amount
     */
    public PaymentOrderDTO createOrder(String creditAccountNumber, String debitAccountNumber, BigDecimal amount){
        PaymentOrderDTO paymentOrderDTO;
        BankAccount creaditAccount = bankAccountRepository.findByAccountNumber(creditAccountNumber);
        BankAccount debitAccount = bankAccountRepository.findByAccountNumber(debitAccountNumber);

        WSResponse wsResponse = new WSResponse(WSResponse.STATUS_ERROR);
        if(debitAccount == null){
            wsResponse.getErrors().add(new WSMessage(WSResponse.ERROR_MESSAGES.ERROR_CODE_008.getCode(),
                WSResponse.ERROR_MESSAGES.ERROR_CODE_008.getMessage()));
        }
        if(amount.compareTo(creaditAccount.getAccountBalance()) > 0){
            wsResponse.getErrors().add(new WSMessage(WSResponse.ERROR_MESSAGES.ERROR_CODE_007.getCode(),
                WSResponse.ERROR_MESSAGES.ERROR_CODE_007.getMessage()));
        }

        if (wsResponse.getErrors().isEmpty()) {
            PaymentOrder paymentOrder = new PaymentOrder();
            paymentOrder.setCreated(LocalDateTime.now());
            paymentOrder.setModified(LocalDateTime.now());
            paymentOrder.setAmount(amount);

            paymentOrder.setPaymentStatus(new PaymentStatus(Constants.PAYMENT_STATUS_CREATED_ID));
            paymentOrder.setCreditAccount(creaditAccount);
            paymentOrder.setDebitAccount(debitAccount);

            paymentOrder = paymentOrderRepository.save(paymentOrder);

            //auto execute an order if the amount is less than 100, leaves every tenth created order unexecuted
            if (paymentOrder.getAmount().compareTo(Constants.AUTO_EXECUTE_CUTOFF_AMOUNT) <= 0 &&
                paymentOrder.getId() % 10 != 0) {
                this.executePaymentOrder(paymentOrder);
            }

            BoundMapperFacade<PaymentOrder, PaymentOrderDTO> mapper = PaymentOrderMapper
                .getMapperFacade(PaymentOrder.class,
                    PaymentOrderDTO.class);
            paymentOrderDTO = mapper.map(paymentOrder);
        }
        else{
            throw new ServiceException(wsResponse);
        }

        return paymentOrderDTO;
    }

    /**
     * Load a payment order with the given id and fetches all the related records.
     * @param orderId
     * @return
     */
    public PaymentOrderDTO getPaymentOrderDetails(Integer orderId){
        PaymentOrder paymentOrder = paymentOrderRepositoryCustom.getPaymentOrderDetails(orderId);
        BoundMapperFacade<PaymentOrder, PaymentOrderDTO> mapper = PaymentOrderMapper.getMapperFacade(PaymentOrder.class,
            PaymentOrderDTO.class);

        return paymentOrder!=null ? mapper.map(paymentOrder) : null;
    }

    /**
     * Executes the given payment order if the credit account balance is grater than or equal to the order amount.
     * Otherwise declines the payment order.
     * @param orderId
     * @return
     */
    public PaymentOrderDTO executePaymentOrder(Integer orderId){
        PaymentOrder paymentOrder = paymentOrderRepositoryCustom.getPaymentOrderDetails(orderId);

        return executePaymentOrder(paymentOrder);
    }

    /**
     * Executes the given payment order if the credit account balance is grater than or equal to the order amount.
     * Otherwise declines the payment order.
     * @param paymentOrder
     * @return
     */
    private PaymentOrderDTO executePaymentOrder(PaymentOrder paymentOrder){
        BankAccount creditAccount = paymentOrder.getCreditAccount();
        BankAccount debitAccount = paymentOrder.getDebitAccount();

        if(creditAccount.getAccountBalance().compareTo(paymentOrder.getAmount()) >= 0){
            paymentOrder.setPaymentStatus(new PaymentStatus(Constants.PAYMENT_STATUS_EXECUTED_ID));
            BigDecimal newCreditBalance = creditAccount.getAccountBalance().subtract(paymentOrder.getAmount());
            creditAccount.setAccountBalance(newCreditBalance);
            BigDecimal newDebitBalance = debitAccount.getAccountBalance().add(paymentOrder.getAmount());
            debitAccount.setAccountBalance(newDebitBalance);
        }
        else{
            paymentOrder.setPaymentStatus(new PaymentStatus(Constants.PAYMENT_STATUS_DECLINED_ID));
        }
        paymentOrder.setModified(LocalDateTime.now());

        BoundMapperFacade<PaymentOrder, PaymentOrderDTO> mapper = PaymentOrderMapper.getMapperFacade(PaymentOrder.class,
            PaymentOrderDTO.class);

        return paymentOrder!=null ? mapper.map(paymentOrder) : null;
    }

    /**
     * Declines the given payment order.
     * @param orderId
     * @return
     */
    public PaymentOrderDTO declinePaymentOrder(Integer orderId){
        PaymentOrder paymentOrder = paymentOrderRepositoryCustom.getPaymentOrderDetails(orderId);
        paymentOrder.setPaymentStatus(new PaymentStatus(Constants.PAYMENT_STATUS_DECLINED_ID));
        paymentOrder.setModified(LocalDateTime.now());

        BoundMapperFacade<PaymentOrder, PaymentOrderDTO> mapper = PaymentOrderMapper.getMapperFacade(PaymentOrder.class,
            PaymentOrderDTO.class);

        return paymentOrder!=null ? mapper.map(paymentOrder) : null;
    }
}
