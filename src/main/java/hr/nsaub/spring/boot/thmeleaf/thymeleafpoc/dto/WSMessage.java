package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto;

import java.io.Serializable;

/**
 * Response message object.
 */
public class WSMessage implements Serializable{
    private static final long serialVersionUID = 1L;

    private String messageCode;
    private String messageString;

    public WSMessage() {
    }

    public WSMessage(String messageCode, String messageString) {
        this.messageCode = messageCode;
        this.messageString = messageString;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessageString() {
        return messageString;
    }

    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }
}
