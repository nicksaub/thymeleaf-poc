package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.mappers;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.PaymentOrderDTO;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.PaymentOrder;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class PaymentOrderMapper {
    private static final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();

    static {
        mapperFactory.classMap(PaymentOrder.class, PaymentOrderDTO.class).byDefault()
            .field("paymentStatus.id", "status")
            .field("paymentStatus.name", "statusName")
            .field("creditAccount.accountNumber", "creditAccount")
            .field("debitAccount.accountNumber", "debitAccount")
            .field("creditAccount.bankUser.userFirstName", "payerFirstName")
            .field("creditAccount.bankUser.userLastName", "payerLastName")
            .field("creditAccount.bankUser.userPhone", "payerPhone")
            .field("creditAccount.bankUser.userEmail", "payerEmail")
            .field("debitAccount.bankUser.userFirstName", "payeeFirstName")
            .field("debitAccount.bankUser.userLastName", "payeeLastName")
            .field("debitAccount.bankUser.userPhone", "payeePhone")
            .field("debitAccount.bankUser.userEmail", "payeeEmail")
            .register();
    }

    private PaymentOrderMapper(){}

    public static <A, B> BoundMapperFacade<A, B> getMapperFacade(Class<A> var1, Class<B> var2){
        return mapperFactory.getMapperFacade(var1, var2);
    }
}
