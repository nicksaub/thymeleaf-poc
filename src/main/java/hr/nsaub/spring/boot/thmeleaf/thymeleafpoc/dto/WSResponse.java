package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Response object with status and messages.
 */
public class WSResponse implements Serializable{
    public static final Short STATUS_OK = 0;
    public static final Short STATUS_ERROR = -1;

    public enum ERROR_MESSAGES{
        ERROR_CODE_001("001", "Username missing!"),
        ERROR_CODE_002("002", "User with a username '%s' already exists!"),
        ERROR_CODE_003("003", "Password is missing!"),
        ERROR_CODE_004("004", "Password must be at least 8 characters long, "
            + "have at least one capital letter and be a mix of letters and digits!"),
        ERROR_CODE_005("005", "Password and the repeated password don't match!"),
        ERROR_CODE_006("006", "E-mail is missing!"),
        ERROR_CODE_007("007", "Payment amount exceeds the accout balance!"),
        ERROR_CODE_008("008", "Debit account not found in the system!"),
        ERROR_CODE_009("009", "Debit account missing!"),
        ERROR_CODE_010("010", "Amount not properly formatted!"),
        ERROR_CODE_999("999", "Account numbers are spent!");

        ERROR_MESSAGES(String code, String message) {
            this.code = code;
            this.message = message;
        }

        private String code;
        private String message;

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }

    public enum INFO_MESSAGES{
        INFO_CODE_001("001", "Order processed."),
        INFO_CODE_002("002", "Order declined.");

        INFO_MESSAGES(String code, String message) {
            this.code = code;
            this.message = message;
        }

        private String code;
        private String message;

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }

    private Short status;
    private List<WSMessage> infos = new ArrayList<>();
    private List<WSMessage> warnings = new ArrayList<>();
    private List<WSMessage> errors = new ArrayList<>();

    public WSResponse() {
    }

    public WSResponse(Short status) {
        this.status = status;
    }

    public WSResponse(Short status, String code, String message) {
        this.status = status;
        errors.add(new WSMessage(code, message));
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public List<WSMessage> getInfos() {
        return infos;
    }

    public void setInfos(List<WSMessage> infos) {
        this.infos = infos;
    }

    public List<WSMessage> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<WSMessage> warnings) {
        this.warnings = warnings;
    }

    public List<WSMessage> getErrors() {
        return errors;
    }

    public void setErrors(List<WSMessage> errors) {
        this.errors = errors;
    }
}
