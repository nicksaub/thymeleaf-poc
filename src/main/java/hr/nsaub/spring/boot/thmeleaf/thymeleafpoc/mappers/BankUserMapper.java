package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.mappers;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.BankUserDTO;
import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.model.BankUser;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * Orika mapper for BankUser and related entities.
 */
public class BankUserMapper {
    private static final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();

    static {
        mapperFactory.classMap(BankUser.class, BankUserDTO.class).byDefault()
            .register();
    }

    private BankUserMapper(){}

    public static <A, B> BoundMapperFacade<A, B> getMapperFacade(Class<A> var1, Class<B> var2){
        return mapperFactory.getMapperFacade(var1, var2);
    }
}
