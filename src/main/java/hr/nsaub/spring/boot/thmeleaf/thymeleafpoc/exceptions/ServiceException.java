package hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.exceptions;

import hr.nsaub.spring.boot.thmeleaf.thymeleafpoc.dto.WSResponse;

/**
 * A custom exception for the service layer with the response object.
 */
public class ServiceException extends RuntimeException{
    private WSResponse wsReponse;

    public ServiceException(WSResponse wsReponse) {
        this.wsReponse = wsReponse;
    }

    public WSResponse getWsReponse() {
        return wsReponse;
    }
}
