insert into message (id, timestamp, status, message) values
  (1, parsedatetime('18.06.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 1');
insert into message (id, timestamp, status, message) values
  (2, parsedatetime('19.06.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 2');
insert into message (id, timestamp, status, message) values
  (3, parsedatetime('20.06.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 3');
insert into message (id, timestamp, status, message) values
  (4, parsedatetime('21.06.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 4');
insert into message (id, timestamp, status, message) values
  (5, parsedatetime('23.07.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 5');
insert into message (id, timestamp, status, message) values
  (6, parsedatetime('24.07.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 6');
insert into message (id, timestamp, status, message) values
  (7, parsedatetime('28.07.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 7');
insert into message (id, timestamp, status, message) values
  (8, parsedatetime('30.07.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 8');
insert into message (id, timestamp, status, message) values
  (9, parsedatetime('08.08.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 9');
insert into message (id, timestamp, status, message) values
  (10, parsedatetime('09.08.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 10');
insert into message (id, timestamp, status, message) values
  (11, parsedatetime('10.08.2018 18:47:52', 'dd.MM.yyyy hh:mm:ss'), 'READ', 'Test message 11');


insert into role (id, name) values (1, 'USER');
insert into role (id, name) values (2, 'ADMIN');

insert into payment_status (id, name) values (1, 'Created');
insert into payment_status (id, name) values (2, 'Executed');
insert into payment_status (id, name) values (3, 'Declined');

insert into bank_user (id, user_name, user_password, user_first_name, user_last_name, user_email, user_phone, role_id)
    values (1, 'banka', '$2a$10$snXK4qwh2UQpn/SLrTi80OD/xMPQowc0rPqPFiG3WEXb5Vo2rdyKi', 'Banka', 'Banka',
        'banka@banka.hr', '01414617', 2);
insert into bank_user (id, user_name, user_password, user_first_name, user_last_name, user_email, user_phone, role_id)
    values (4, 'nicksaub', '$2a$10$klqAAhRuP41rpI1nNBrHI.Uz6j09WCfXgARXfoGlhL3BlaP1SPVbq', 'Nikola', 'Šaub',
        'nicksaub@gmail.com', '01414617', 1);

insert into bank_account (id, account_number, account_balance, user_id)
    values (3, '10000000002', 10000.0, 4);
insert into bank_account (id, account_number, account_balance, user_id)
    values (4, '10000000003', 1000000000.0, 1);

insert into payment_order (id, amount, created, modified, payment_status_id, credit_account_id, debit_account_id)
    values (4, 200.0, '2018-10-29 09:46:34', '2018-10-29 09:46:34', 1, 3, 4);
insert into payment_order (id, amount, created, modified, payment_status_id, credit_account_id, debit_account_id)
    values (5, 250.0, '2018-10-30 09:46:34', '2018-10-30 09:46:34', 2, 3, 4);
insert into payment_order (id, amount, created, modified, payment_status_id, credit_account_id, debit_account_id)
    values (6, 1250.0, '2018-10-31 09:46:34', '2018-10-31 09:46:34', 3, 3, 4);


create sequence account_number_sequence start with 1;