$(document).ready(function () {
    /**
     * Displays a form for message input in a popup.
     */
    $("#addMessageButton").on("click", function (event) {
        $(".messageForm #messageFormModal").modal();
    });

    /**
     * Displays a form for order input in a popup.
     */
    $("#addOrderButton").on("click", function (event) {
        $(".orderForm #orderFormModal").modal();
    });

    $(".messageDatePicker").datepicker({"format" : "dd.mm.yyyy.", "autoclose" : true});

    $('[data-toggle="tooltip"]').tooltip();
});

/**
 * Triggered when a user click a row in the messages table. Marks a message as selected on the server side.
 * @param row
 * @param messageId
 */
function handleMessageRowClick(row, messageId) {
    row.addClass('bg-info').siblings().removeClass('bg-info');
    $.get("/messages/handleSelect", {'selectedMessageId' : messageId}, function (data) {
        if(data.status == 0){
            $("#deleteMessageButton").show();
        }
    });
}

function handleDateEdit(cell){
    cell.find(".messageDateLabel").hide();
    cell.find(".messageDatePicker").show();
    cell.find(".messageDatePickerClose").show();
}

function handleDateChange(datePicker, messageId){
    $.post("/messages/updateDate", {'selectedMessageId' : messageId, "selectedDate" : datePicker.val()}, function (data) {
        if(data.status == 0){
            datePicker.hide();
            var messageDateLabel = datePicker.parent().parent().find(".messageDateLabel");
            messageDateLabel.html(datePicker.val());
            messageDateLabel.show();
            datePicker.parent().parent().find(".messageDatePickerClose").hide();
        }
    });
}

function handleDatePickerClose(event, button){
    event.stopImmediatePropagation();
    button.hide();
    button.parent().parent().find(".messageDatePicker").hide();
    button.parent().parent().find(".messageDateLabel").show();
}